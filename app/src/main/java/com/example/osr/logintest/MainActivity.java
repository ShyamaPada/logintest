package com.example.osr.logintest;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;


public class MainActivity extends AppCompatActivity {

        TextView textViewName;
        CallbackManager callbackManager;
        ProfileTracker profileTracker;
        AccessTokenTracker accessTokenTracker;
        FacebookCallback facebookCallback;
        ImageView imageView;
        LoginButton loginButton;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
            loginButton = (LoginButton)findViewById(R.id.login_button);
            textViewName=(TextView)findViewById(R.id.textView_Login) ;
            imageView=(ImageView)findViewById(R.id.imageView_Login);
           /* accessTokenTracker=new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                           AccessToken currentAccessToken) {

                }
            };
            profileTracker=new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                    displayMessage(currentProfile);

                }
            };
            accessTokenTracker.startTracking();
            profileTracker.startTracking();*/


            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    AccessToken accessToken=loginResult.getAccessToken();
                    Profile profile=Profile.getCurrentProfile();
                    textViewName.setText("Success");
                    displayMessage(profile);

                }


                @Override
                public void onCancel() {

                    textViewName.setText("Login Canceled");
                }

                @Override
                public void onError(FacebookException error) {
                    textViewName.setText("Error occured");

                }
            });
            loginButton.setReadPermissions("user_friends");
            loginButton.registerCallback(callbackManager,facebookCallback);

        }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode,resultCode,data);

            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        public void displayMessage(Profile profile){
            if(profile!=null){
                textViewName.setText(profile.getName());
                String url=profile.getProfilePictureUri(200,200).toString();

                Glide.with(getApplicationContext()).load(url).error(R.mipmap.ic_launcher).
                        into(imageView);


            }
        }
    }



